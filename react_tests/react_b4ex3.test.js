import { configure, mount } from "enzyme";
import "@testing-library/jest-dom";
import App from "../App.js";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import { act } from "@testing-library/react";

configure({ adapter: new Adapter() });

const app = mount(<App />);

describe("b4ex03: Input validation", () => {
  it("App.js has form, 2 inputs and 1 button", () => {
    if (
      !app.find("form").props() ||
      app.find("input").length !== 2 ||
      !app.find("button")
    )
      return false;
  });
  it("Inputs or form have onChange event and form has onSubmit event", () => {
    let correctEvents = true;

    app.find("input").forEach((input) => {
      if (
        input.prop("onChange") === undefined &&
        app.find("form").props().onChange === undefined
      )
        correctEvents = false;
    });

    if (app.find("form").props().onSubmit === undefined) correctEvents = false;

    if (!correctEvents) return false;
  });
  it("If no match, you should render `Match not found`", () => {
    let matchFound = false;

    app.find("input").forEach((input, idx) => {
      idx === 0
        ? input.simulate("change", { target: { value: "a@a.com" } })
        : idx === 1 && input.simulate("change", { target: { value: "b" } });
    });
    app.find("button").simulate("submit");

    app
      .children()
      .findWhere((x) => x.text().toLowerCase().includes("match"))
      .forEach((child) => {
        if (child.props().children?.includes("not found")) matchFound = true;
      });

    if (!matchFound) return false;
  });
  it("If match, you should render `Match found successfully!`", () => {
    cleanUpInputs();
    fillCorrectly();
    app.find("form").simulate("submit");
    if (!app.find("h2").text().toLowerCase().includes("successfully"))
      return false;
  });
  it("If match, message should be green", () => {
    cleanUpInputs();
    fillCorrectly();
    app.find("form").simulate("submit");
    if (app.find("h2").props().style.color !== "green") return false;
  });
  it("If no match, message should be red", () => {
    cleanUpInputs();
    fillIncorrectly();
    app.find("form").simulate("submit");
    if (app.find("h2").props().style.color !== "red") return false;
  });
  it("Renders disappearing messages", async () => {
    cleanUpInputs();
    fillCorrectly();
    app.find("form").simulate("submit");
    if (!app.find("h3").text().includes(3))
      throw new Error("Somthing went wrong");
    await waitOneSecond();
    if (!app.find("h3").text().includes(2))
      throw new Error("Somthing went wrong");
    await waitOneSecond();
    if (!app.find("h3").text().includes(1))
      throw new Error("Somthing went wrong");
    await waitOneSecond();
    if (!app.find("h3").text().includes(0))
      throw new Error("Somthing went wrong");
    await waitOneSecond();
    if (app.find("h3").text() !== "") throw new Error("Somthing went wrong");
  });
});

const waitOneSecond = async () => {
  await act(async () => {
    await new Promise((resolve) => setTimeout(resolve, 1000));
    app.update();
  });
};

const fillCorrectly = () => {
  app.find("input").forEach((input) => {
    input.simulate("change", {
      target: {
        value:
          input.props().name === "email"
            ? "a@a.com"
            : input.props().name === "password" && "a",
        name: input.props().name,
      },
    });
  });
};

const fillIncorrectly = () => {
  app.find("input").forEach((input) => {
    input.simulate("change", {
      target: {
        value:
          input.props().name === "email"
            ? "a@a.com"
            : input.props().name === "password" && "b",
        name: input.props().name,
      },
    });
  });
};

const cleanUpInputs = () => {
  const inputs = app.find("input");
  inputs.forEach((input) =>
    input.props().name === "email"
      ? input.simulate("change", { target: { value: "", name: "email" } })
      : input.props().name === "password" &&
      input.simulate("change", { target: { value: "", name: "password" } })
  );
};

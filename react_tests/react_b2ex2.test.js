import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import "@testing-library/jest-dom";
import App from "../App.js";
import { isClassComponent } from "./utils.js";

configure({ adapter: new Adapter() });

const children = shallow(<App />).props().children;

describe("b2ex02: Display name and surname side by side", () => {
  it("App.js must render 2 class components", () => {
    let allClassComponents = false;

    children.forEach((child, i) => {
      child.props.children.forEach((component) => {
        if (isClassComponent(component)) allClassComponents = true;
      });
    });

    if (!allClassComponents) return false;
  });
  it("Render name and surname side by side", () => {
    let nameAndSurnameDisplayedCorrectly = false;

    children.forEach((child, i) => {
      if (
        child.props.children[0].props.name &&
        child.props.children[1].props.surname
      ) {
        nameAndSurnameDisplayedCorrectly = true;
      } else nameAndSurnameDisplayedCorrectly = false;
    });

    if (!nameAndSurnameDisplayedCorrectly) return false;
  });
});

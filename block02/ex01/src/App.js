import React from 'react';
import Child from './Child';

class App extends React.Component{
  render(){
    const myName='J.D.';
    return(
      <div>
        <Child name={myName}/>
      </div>
    )
  }
}

export default App;
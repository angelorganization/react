import React from 'react';
import List from './List';


class App extends React.Component{
  render(){
    const categories=['shirts','hats','shoes'];
    return(
      <div>
        <List categories={categories}/>
      </div>
    )
  }
}

export default App;
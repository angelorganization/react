import React from 'react';
import Item from './Item';

class List extends React.Component{
  render(){
  	console.log(this.props)
    return(
      	<ul>
      		{this.props.categories.map((cat,i)=><Item category={cat} key={i}/>)}
		</ul>
    )
  }
}

export default List;
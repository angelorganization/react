import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import "@testing-library/jest-dom";
import App from "../App.js";
import List from "../List.js";
import { isFunctionalComponent } from "./utils.js";

configure({ adapter: new Adapter() });

const children = shallow(<App />).props().children;
const list = shallow(<List categories={["shoes", "hats", "shirts"]} />);
const listWithoutProps = shallow(<List categories={[]} />);

describe("b2ex04: Display categories ( Functional )", () => {
  it("App.js must render functional component List", () => {
    let listIsFunctionalComponent = false;

    if (isFunctionalComponent(children)) listIsFunctionalComponent = true;

    if (!listIsFunctionalComponent) return false;
  });
  it("List must render functional component Item", () => {
    let itemIsFunctionalComponent = false;

    list.props().children.forEach((child) => {
      if (!`${child.type}`.toLowerCase().includes("class"))
        itemIsFunctionalComponent = true;
    });

    if (!itemIsFunctionalComponent) return false;
  });
  it("Pass categories array from App.js to List.js", () => {
    let categoriesPassedAsProps = false;

    if (listWithoutProps.props().children.length === 0)
      categoriesPassedAsProps = true;

    if (!categoriesPassedAsProps) return false;
  });
  it("Pass category item from List.js to Item.js", () => {
    let categoryPassedAsProps = false;

    list.props().children.forEach((child) => {
      if (Object.keys(child.props)[0] === "category")
        categoryPassedAsProps = true;
    });

    if (!categoryPassedAsProps) return false;
  });
});

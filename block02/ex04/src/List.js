import React from 'react';
import Item from './Item';

const List=(props)=>{
	console.log(props)
  return(
  	<ul>
  		{props.categories.map((cat,i)=><Item category={cat}key={i}/>)}
	  </ul>
  )
}

export default List;
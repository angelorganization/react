import React from 'react';
import List from './List';

const App=()=>{
  const categories=['shirts','hats','shoes'];
  return(
    <div>
      <List categories={categories}/>
    </div>
  )
}

export default App;
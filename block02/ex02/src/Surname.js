import React from 'react';

class Surname extends React.Component{
  render(){
    return(
      <span>{this.props.surname}</span>
    )
  }
}

export default Surname;
import React from 'react';

class Name extends React.Component{
  render(){
    return(
      <span>{this.props.name}</span>
    )
  }
}

export default Name;
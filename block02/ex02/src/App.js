import React from 'react';
import Name from './Name';
import Surname from './Surname';


class App extends React.Component{
  render(){
    let names = ["Clifford", "Russel", "Michael", "Dennis", "Andre"];
    let surnames = ["Smith", "Simmons", "Diamond", "Coles", "Benjamin"];
    let fullNames=names.map((name,i)=>{
        return(
          <div key={i}>
            <Name name={name}/>
            <Surname surname={surnames[i]}/>
          </div>
        );
      });
    return(
      <div>
        {fullNames}
      </div>
    )
  }
}

export default App;
import React from 'react';

const SingleProduct=(props)=>{
  console.log(props)
  return(
  	<li>
        <h3>{props.prod.product}</h3>
        <h3>{'$'+props.prod.price}</h3>
        {props.prod.onSale && <h2>ON SALE</h2>}
        <img alt={props.prod.product} src={props.prod.image}></img>
    </li>
  )
}

export default SingleProduct;
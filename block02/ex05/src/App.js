import React from 'react';

import products from './products';
import ProductsList from './ProductsList';
import './App.css'

const App=()=>{
  return(
    <>
      <header>
        <h1>My Shop | Hello World!</h1>
      </header>
      <h1>⭐~Best Sellers~⭐</h1>
      <ProductsList products={products.filter(prod=>prod.bestSeller)}/>
      <h1>All Products</h1>
      <ProductsList products={products}/>
      <footer>
        <h1>All rights reserved | Angel Organization 2022</h1>
      </footer>
    </>
  )
}

export default App;
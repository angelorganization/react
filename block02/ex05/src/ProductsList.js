import React from 'react';

import SingleProduct from './SingleProduct';

const ProductsList=(props)=>{
  return(
  	<ul className='productsList'>
      {props.products.map((prod,i)=> <SingleProduct prod={prod} key={i}/>)}
  	</ul>
    )
}



export default ProductsList;
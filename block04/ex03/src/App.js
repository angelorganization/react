import React,{useEffect, useState} from 'react';

import './App.css';
import Message from './Message';

const App=()=>{
  const [email,setEmail]=useState('');
  const [password,setPassword]=useState('');
  const [msgObj,setMsgObj]=useState({shown:'',success:'Match found successfully',failure:'Match not found',cssClass:'green'});
  const [stateCount,setStateCount]=useState(3);
  const [intervalGoing,setIntervalGoing]=useState(false);
  const users = [
    { email: "a@a.com", password: "a" },
    { email: "b@b.com", password: "b" },
    { email: "c@c.com", password: "c" },
  ]
  let id;
  let temp=msgObj;
  let count=3;
  const handleSubmit=(e)=>{
    e.preventDefault();
    const emailIdx=users.findIndex((obj)=>obj.email===email);
    const passwordIdx=users.findIndex((obj)=>obj.password===password);
    if(emailIdx!=-1&&passwordIdx!=-1&&emailIdx===passwordIdx){
      makeMsg('success','green');
    }else{
      makeMsg('failure','red');
    }
    if(intervalGoing){
      console.log(id);
      clearInterval(id);
      // intervalIDs.forEach((id)=>id.pop());
      setStateCount(3);
      count=3;
      console.log('cleared');
    }
    startInterval();
  }
  const makeMsg=(result,color)=>{
    temp.shown=temp[result];
    temp.cssClass=color;
    setMsgObj({...temp});
  }
  const startInterval=()=>{
    id=setInterval(()=>{
      setIntervalGoing(true);
      count--;
      setStateCount(count);
      if(count===-1){
        temp.shown='';
        stopInterval();
        setMsgObj({...temp});
        setStateCount(3);
        setIntervalGoing(false);
      };
    },1000);
    // intervalIDs.forEach((id)=>id.pop());
    // intervalIDs.push(id);
  }
  const stopInterval=()=>{
    clearInterval(id);
    id=null;
  }
  return(
    <>
    <form onSubmit={(e)=>handleSubmit(e)}>
      <input name='email' type='email' onChange={(e)=>setEmail(e.target.value)}/>
      <input name='password' type='password' onChange={(e)=>setPassword(e.target.value)}/>
      <button>Click me!</button>
    </form>
    <Message items={{msgObj,stateCount}}/>
    </>
  )
}

export default App;

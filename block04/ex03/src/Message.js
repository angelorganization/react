import React,{useState,useEffect} from 'react';

const Message=(props)=>{
    return(
        <>
            <h2 className={props.items.msgObj.cssClass}>{props.items.msgObj.shown}</h2>
            {props.items.msgObj.shown&&<h3>This message will disappear in {props.items.stateCount}</h3>}
        </>

    )
}

export default Message;

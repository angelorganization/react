import React, {useState, useEffect} from 'react';

const App=()=>{
  const [rate,setRate]=useState(0);
  const [result,setResult]=useState('');
  const apiKey='f1368bc4010561554837a74f151623e5';
  const url=`http://api.currencylayer.com/live?access_key=${apiKey}&currencies=USD,EUR`;
  useEffect(()=>{
    fetch(url)
      .then((res)=>res.json())
      .then((data)=>setRate(data.quotes.USDEUR))
      .catch((error)=>console.log(error));
  },[]);
  return(
    <>
      <input type='number' onChange={(e)=>setResult(e.target.value/rate)}/><span> EUR</span>
      <h1>{result} USD</h1>
    </>
  )
}

export default App;

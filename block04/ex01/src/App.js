import React,{useState} from 'react';

const App=()=>{
  const [counter,setCounter]=useState(0);
  /*const handleClick=()=>{
    setCounter(counter+1);
  }
  if(counter%2===0){
    if(counter===0){
      return(
        <div>
          <h2>{counter}</h2>
          <button onClick={handleClick}>INCREASE COUNTER</button>
        </div>
      )
    }else{
      return(
        <div>
          <h2>{counter-1}</h2>
          <button onClick={handleClick}>INCREASE COUNTER</button>
        </div>
      )
    }
  }else{
    return(
    <div>
      <h2>{counter}</h2>
      <button onClick={handleClick}>INCREASE COUNTER</button>
    </div>
    )
  }*/
  // MY CODE ^^^^
  // REFACTORED CODE ⬇️⬇️⬇️
  return(
    <div>
      {counter%2===0&&counter!==0?<h2>{counter-1}</h2>:<h2>{counter}</h2>}
      <button onClick={()=>setCounter(counter+1)}>INCREASE COUNTER</button>
    </div>
  )
}

export default App;

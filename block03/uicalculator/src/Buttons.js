import React from 'react';

const Buttons=(props)=>{
    const btns=props.items.btns;
    const handleClick=(obj,i)=>{
        props.items.getData(obj,i);
    }
    return(
        btns.map((obj,i)=>{
            return <h2 key={i} className={obj.cssClass} onClick={()=>handleClick(obj,i)}>{obj.val}</h2>
        })
    )
}

export default Buttons;

import React from 'react';

const FirstLine=(props)=>{
    const handleClick=(e,char)=>{
        let operatorElems=[];
        for(let i=1;i<5;i++){operatorElems.push(e.target.parentNode.parentNode.children[i].children[3]);}
        if(char==='C'){props.items.getData(char,operatorElems);}
        else if(char==='\u00f7'){
            for(let i=0;i<4;i++){
                if(i===0){
                    operatorElems[i].style.color='red';
                    props.items.getOp('\u00f7',operatorElems);
                }
                else{operatorElems[i].style.color='black';}
            }
        }else{props.items.getData(char);}
    }
    const btns=props.items.btns;
    return(
            <div className='buttons'>
                <h2 onClick={(e)=>handleClick(e,btns[0])}>{btns[0]}</h2>
                <h2 onClick={(e)=>handleClick(e,btns[1])}>{btns[1]}</h2>
                <h2><img src='Assets/Cat.png' alt='Image of a Kitty Cat'/></h2>
                <h2 onClick={(e)=>handleClick(e,btns[3])}>{btns[3]}</h2>
            </div>
    )
}

export default FirstLine;

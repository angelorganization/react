import React from 'react';

import {evaluate} from 'mathjs';

const NumberButtons=(props)=>{
    const handleClick=(e,char)=>{
        let operatorElems=[];
        for(let i=1;i<5;i++){
            operatorElems.push(e.target.parentNode.parentNode.children[i].children[3]);
        }
        if(char==='x'|char==='+'|char==='-'){
            const operatorClicked=operatorElems.findIndex((elem)=>elem===e.target)
            for(let i=0;i<4;i++){
                if(i===operatorClicked){
                    operatorElems[i].style.color='red';
                    props.items.getOp(char);
                }else{
                    operatorElems[i].style.color='black';
                }
            }
        }else{
            props.items.getData(char,operatorElems);
        }

    }
    const btns=props.items.btns;
    return(
        // this map function is going through the list of charber sets [1,2,3,'x'] etc. 
        // and in total making three divs, each with 4 buttons in them going from left to right
        btns.map((charSet, i)=>{
                return(
                <div className='buttons' key={i}>
                    <h2 key={i} className={ele.cssClass} onClick={(e)=>handleClick(e,charSet[0], i)}>{charSet[0]}</h2>
                    <h2 onClick={(e)=>handleClick(e,charSet[1])}>{charSet[1]}</h2>
                    <h2 onClick={(e)=>handleClick(e,charSet[2])}>{charSet[2]}</h2>
                    <h2 onClick={(e)=>handleClick(e,charSet[3])}>{charSet[3]}</h2>
                </div>
            )   
        })
    )
}

export default NumberButtons;

//Notes: When doing pressing the action button that isn't equals twice in a row it breaks

import React,{useState, useEffect} from 'react';
import {evaluate} from 'mathjs';

import './App.css';
import Display from './Display.js';
import buttons from './buttonsArray.js';
import Buttons from './Buttons.js';
const App=()=>{
  const [shown, setShown]=useState('0');
  const [num1, setNum1]=useState(0);
  const [num2,setNum2]=useState(0);
  const [op, setOp]=useState('');
  const [num1Locked, setNum1Lock]=useState(false);
  const [num2Locked, setNum2Lock]=useState(true);
  const [answer, setAnswer]=useState('');
  const [btns,setBtns]=useState(buttons);
  const [bleh,updateRender]=useState(null);

  const getData=(obj,i)=>{
    if(obj.val===Number(obj.val)){
      numPressed(obj,i);
    }else{
      charPressed(obj,i);
    }
  }
  const numPressed=(obj,i)=>{
    let num;
    //if num1 is being entered
    if(!num1Locked){
      num=Number(String(num1)+String(obj.val));
        if(answer!=''){
          setAnswer('')
        }
        if(String(num).length<5){
          setNum1(num);
          changeShown(num);
        }
    //else num2 is being entered
    }else{
      setNum2Lock(false);
      num=Number(String(num2)+String(obj.val));
        if(String(num).length<5){
          setNum2(num);
          changeShown(num);
        }
    }
  }
  const charPressed=(obj,i)=>{
    if(obj.val=='\u00f7'||obj.val=='x'||obj.val=='-'||obj.val=='+'){
      allOpsBlack();
      obj.cssClass='red';
      updateRender(obj.val);
      //sets the num1 as locked here because once an operator has been pressed, even if the operator is changed,
      //it will still start doing num2 once another number is pressed
      setNum1Lock(true);
      if(obj.val==='\u00f7'){setOp('/')}
      else if(obj.val==='x'){setOp('*')}
      else{setOp(obj.val);}
      //if an operator has already been pressed and the second number has begun to be typed
      if(op!=''&&num2Locked==false){
        calculate();
      //calcuulators have a lot of corner cases, if the recently calculated answer is on the screen and the operator is directly pressed, set the answer to num1
      }else if(answer===shown&&num2Locked){
        setNum1(answer);
      }
    }else if(obj.val==='C'){
      setNum1(0);
      setNum2(0);
      allOpsBlack();
      changeShown(0);
    }else if(obj.val==='='){
      num1Locked&&!num2Locked && calculate();
    }
  }
  const changeShown=(num)=>{
    setShown(num);
  }
  const allOpsBlack=()=>{
    let arr=['\u00f7','x','-','+'];
    for(var elem of arr){
      btns[btns.findIndex((obj)=>obj.val===elem)].cssClass='black';
    }
  }
  const calculate=()=>{
    allOpsBlack();
    let ans=evaluate(String(num1)+op+String(num2));
    setAnswer(ans);
    console.log('answer',ans);
    console.log('num1',num1);
    console.log('num2',num2);
    changeShown(ans);
    setNum2Lock(true);
    setNum1Lock(false);
    setNum1(0);
    setNum2(0);
    setOp('');
  }
  return(
    <>
      <Display shown={shown}/>
      <div className='buttonsGrid'>
        <Buttons items={{btns:btns,getData}}/>
      </div>
    </>
  )
}

export default App;

import React from 'react';

const LastLine=(props)=>{
    const handleClick=(char)=>{
        if(char==0){
            props.items.getData(char);
        }else if(char=='='){
            props.items.calculate();
        }
    }
    const btns=props.items.btns;
    return(
        <div className='lastLineButtons'>
            <div>
                <h2 className='zero' onClick={()=>handleClick(btns[0])}>{btns[0]}</h2>
            </div>
            <div className='twoRows'>
                <h2 onClick={()=>handleClick(btns[1])}>{btns[1]}</h2>
                <h2 onClick={()=>handleClick(btns[2])}>{btns[2]}</h2>
            </div>
        </div>
        )
}

export default LastLine;

import React, {useState} from 'react';

const App=()=>{
  const [shown, setShown]=useState('no data provided!');

  const handleOnChange=e=>{
    if(e.target.value===''){
      setShown('no data provided!');
      // return 0;
    }
    setShown(e.target.value);
  }
    return(
      <div>
        <input onChange={handleOnChange}/>
        <h1>{shown}</h1>
      </div>
    )
  }

export default App;
import { configure, shallow } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import "@testing-library/jest-dom";
import App from "../App.js";

configure({ adapter: new Adapter() });

const app = shallow(<App />);

describe("b3ex02: Change and render state ( Functional )", () => {
  it("There should be an input in App.js with an onChange event", () => {
    let isThereInputWithOnChangeEvent = false;
    const input = app.find("input");
    if (input.prop("onChange") !== undefined) {
      isThereInputWithOnChangeEvent = true;
    }

    if (!isThereInputWithOnChangeEvent) return false;
  });
  it("There should be an h1 component initialised to 'no data provided!'", () => {
    let h1InitialisedCorrectly = false;

    const h1 = app.find("h1");
    if (h1.html().toLowerCase().includes("no data provided"))
      h1InitialisedCorrectly = true;

    if (!h1InitialisedCorrectly) return false;
  });
  it("There should be an h1 component rendering state", () => {
    let h1RenderingState = false;

    const input = app.find("input");
    input.simulate("change", { target: { value: "hello" } });
    if (app.find("h1").html().includes("hello")) h1RenderingState = true;

    if (!h1RenderingState) return false;
  });
});

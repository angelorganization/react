import React from 'react';

class App extends React.Component{
  state={
    shown:'no data provided!',
  }
  handleOnChange=e=>{
    if(e.target.value==''){
      this.setState({shown:'no data provided!'})    
      return 0;
    }
    this.setState({shown:e.target.value});
  }
  render(){
    return(
      <div>
        <input onChange={this.handleOnChange}/>
        <h1>{this.state.shown}</h1>
      </div>
    )
  }
}

export default App;
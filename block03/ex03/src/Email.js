import React from 'react';

const Email=(props)=>{
  const handleChange=e=>{
    props.getEmail(e.target.value);
    console.log(e.target.value);
  }
  return(
  	<input onChange={handleChange}/>
  )
}

export default Email;
import React from 'react';

import Email from './Email.js';
import Password from './Password.js';
import Alert from './Alert.js';

const App=()=>{
  const [email, setEmail]=React.useState();
  const [password, setPassword]=React.useState();
  const getEmail=(email)=>{setEmail(email)};
  const getPassword=(password)=>{setPassword(password)};
  return(
    <div>
      <Email getEmail={getEmail}/>
      <Password getPassword={getPassword}/>
      <Alert data={{email, password}}/>
    </div>
  )
}

export default App;
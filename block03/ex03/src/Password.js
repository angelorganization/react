import React from 'react';

const Password=(props)=>{
  
  const handleChange=e=>{
    props.getPassword(e.target.value);
    console.log(e.target.value)
  }
  return(
  	<input onChange={handleChange}/>
  )
}

export default Password;
import React from 'react';

import products from './products';

const App=()=>{
  const renderProducts=(data, type='all')=>{
    let productsList=data.map((prod,i)=>{
      return(
        <div style={styles.inlineDiv} key={i}>
          <h3>{prod.product}</h3>
          <h3>{'$'+prod.price}</h3>
          {prod.onSale && <h2>ON SALE</h2>}
          <img style={styles.img} alt='flash t-shirt' src={prod.image}></img>
        </div>
      );
    });
    if(type=='best'){
      let j=0;
      for(let i=0;i<products.length;i++){
        if(!products[i].bestSeller){
          productsList.splice(i,1);
          j--;
        }
        j++;
      }
    }
    return productsList;
  }

  return(
    <>
      <header>
        <h1>My Shop | Hello World!</h1>
      </header>
      <h1>⭐~Best Sellers~⭐</h1>
      {renderProducts(products, 'best')}
      <h1>All Products</h1>
      {renderProducts(products, 'all')}
      <footer>
        <h1>All rights reserved | Angel Organization 2022</h1>
      </footer>
    </>
  );
}

const styles={
  img:{
    width:'100px'
  },
  inlineDiv:{
    display:'inline-block'
  }
}

export default App;

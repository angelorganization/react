import React from "react";

const App=()=>{
  let cats=['t-shirts', 'hats', 'shorts', 'jackets', 'pants', 'shoes'];
  let catElems=cats.map((cat,i)=><p key={i}>{cat}</p>);

  return catElems
}

export default App;

